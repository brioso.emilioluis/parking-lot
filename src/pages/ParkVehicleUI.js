import React, { Fragment } from "react";
import { Container, Row, Col } from "react-grid-system";
import VehicleTypeCard from "../components/VehicleTypeCard";

import imageSmall from "../images/small.png";
import imageMedium from "../images/medium.png";
import imageLarge from "../images/large.png";

const textSmall =
  "Small vehicles such as sedans, coupe, convertibles (1.52m x 18m)";
const textMedium = "Medium vehicles such as SUVs, MPVs (1.52m x 23m)";
const textLarge =
  "Large vehicles such as large SUVs, pickup trucks, vans (1.52m x 24m and above)";

const ParkVehicleUI = () => {
  // Render Page
  return (
    <Container class='container-top'>
      <Row>
        <h1>Park Vehicle</h1>
      </Row>
      <Row>
        <h3>Select Vehicle Type:</h3>
      </Row>
      <Row>
        <Col sm={4}>
          <VehicleTypeCard size="small" image={imageSmall} text={textSmall} />
        </Col>
        <Col sm={4}>
          <VehicleTypeCard
            size="medium"
            image={imageMedium}
            text={textMedium}
          />
        </Col>
        <Col sm={4}>
          <VehicleTypeCard size="large" image={imageLarge} text={textLarge} />
        </Col>
      </Row>
    </Container>
  );
};

export default ParkVehicleUI;
