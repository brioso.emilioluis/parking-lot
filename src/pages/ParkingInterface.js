import React, { Fragment, useEffect, useState } from "react";
import { Container, Row, Col } from "react-grid-system";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Button } from "react-bootstrap";

const ParkingInterface = () => {
  // declare redux hooks
  const entryPoints = useSelector((state) => state.systemInfo.entryPoints);
  const parkingSlots = useSelector((state) => state.systemInfo.parkingSlots);
  const smallVehicles = useSelector((state) => state.systemInfo.smallVehicles);
  const mediumVehicles = useSelector(
    (state) => state.systemInfo.mediumVehicles
  );
  const largeVehicles = useSelector((state) => state.systemInfo.largeVehicles);

  const navigate = useNavigate();
  // Park Vehicle
  const parkVehicle = (e) => {
    navigate("/parkVehicle");
  };

  // Unpark Vehicle
  const unparkVehicle = (e) => {
    navigate("/unparkVehicle");
  };

  // Render Parking Interface Page
  return (
    <div class="center">
      <Container>
        <Row>
          <h1>Welcome to Parking</h1>
        </Row>
        <Row>
          <h2> There are currently {parkingSlots} parking slots available</h2>
        </Row>
        <Row>
          <h3> What would you like to do?</h3>
        </Row>
        <Row>
          <Button
            variant="primary"
            type="submit"
            onClick={(e) => parkVehicle(e)}
            id='button'
          >
            Park Vehicle
          </Button>
          <Button
            variant="danger"
            type="submit"
            onClick={(e) => unparkVehicle(e)}
            id='button'
          >
            Unpark Vehicle
          </Button>
        </Row>
      </Container>
    </div>
  );
};

export default ParkingInterface;
