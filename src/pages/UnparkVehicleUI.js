import React from "react";
import ParkedVehiclesTable from "../components/ParkedVehiclesTable";
const UnparkVehicleUI = () => {
  return (
    <div class='container-top'>
      <ParkedVehiclesTable />
    </div>
  );
};

export default UnparkVehicleUI;
