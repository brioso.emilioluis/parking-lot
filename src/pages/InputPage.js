import React, { Fragment, useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { initiate } from "../store/intiate-slice";

const InputPage = () => {
  // declare redux hooks
  const dispatch = useDispatch();

  // declare react hooks
  const [entryPoints, setEntryPoints] = useState(null);
  const [parkingSlots, setParkingSlots] = useState(null);
  const [smallVehicles, setSmallVehicles] = useState(null);
  const [mediumVehicles, setMediumVehicles] = useState(null);
  const [largeVehicles, setLargeVehicles] = useState(null);

  // sets parkingSlots to sum of vehicle types
  useEffect(() => {
    if (smallVehicles && mediumVehicles && largeVehicles) {
      setParkingSlots(
        parseInt(smallVehicles) +
          parseInt(mediumVehicles) +
          parseInt(largeVehicles)
      );
    }
  }, [smallVehicles, mediumVehicles, largeVehicles]);

  // Submits system information to local storage and sets init in redux to true
  const submitSystem = (e) => {
    e.preventDefault();
    const systemInfo = {
      entryPoints,
      parkingSlots,
      smallVehicles,
      mediumVehicles,
      largeVehicles,
    };
    localStorage.setItem("systemInfo", JSON.stringify(systemInfo));
    dispatch(initiate());
  };

  // Render Input Page
  return (
    <div class="container-top">
      <Fragment>
        <h1>Hi! Please help us setup the parking lot system</h1>
        <Form onSubmit={(e) => submitSystem(e)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Number of entry points:</Form.Label>
            <Form.Control
              type="number"
              min="3"
              max="5"
              value={entryPoints}
              onChange={(e) => setEntryPoints(parseInt(e.target.value))}
              placeholder="Entry points (3 to 5)"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Number of parking spots</Form.Label>
            <Form.Control
              type="number"
              disabled={true}
              value={parkingSlots}
              onChange={(e) => setParkingSlots(parseInt(e.target.value))}
              placeholder="Parking spots (maximum 45)"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Small Vehicle parking spots</Form.Label>
            <Form.Control
              type="number"
              min="4"
              max="15"
              value={smallVehicles}
              onChange={(e) => setSmallVehicles(parseInt(e.target.value))}
              placeholder="Small Vehicles (4 to 15)"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Medium Vehicle parking spots</Form.Label>
            <Form.Control
              type="number"
              min="4"
              max="15"
              value={mediumVehicles}
              onChange={(e) => setMediumVehicles(parseInt(e.target.value))}
              placeholder="Medium Vehicles (4 to 15)"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Large Vehicle parking spots</Form.Label>
            <Form.Control
              type="number"
              min="4"
              max="15"
              value={largeVehicles}
              onChange={(e) => setLargeVehicles(parseInt(e.target.value))}
              placeholder="Large Vehicles (4 to 15)"
            />
          </Form.Group>

          <Button variant="primary" type="submit">
            Confirm
          </Button>
        </Form>
      </Fragment>
    </div>
  );
};

export default InputPage;
