import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import { initiate, restart } from "./store/intiate-slice";
import { setSystemInfo, resetSystemInfo } from "./store/system-info-slice";
import { Container } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

// import pages
import InputPage from "./pages/InputPage.js";
import ParkingPage from "./pages/ParkingInterface.js";
import ParkVehicleUI from "./pages/ParkVehicleUI.js";
import UnparkVehicleUI from "./pages/UnparkVehicleUI.js";

function App() {
  // declare redux hooks
  const dispatch = useDispatch();
  const isInitiated = useSelector((state) => state.initiate.isInitiated);

  useEffect(() => {
    // to set isInitiated to true if localStorage has values else restart
    let systemInfo = JSON.parse(localStorage.getItem("systemInfo"));
    if (systemInfo) {
      dispatch(initiate());
      dispatch(setSystemInfo(systemInfo));
    } else {
      dispatch(restart());
    }

    // set systemInfo from new redux
    const smallVehiclesArray = JSON.parse(localStorage.getItem("smallVehicles"))
      ? JSON.parse(localStorage.getItem("smallVehicles"))
      : [];
    const mediumVehiclesArray = JSON.parse(
      localStorage.getItem("mediumVehicles")
    )
      ? JSON.parse(localStorage.getItem("mediumVehicles"))
      : [];
    const largeVehiclesArray = JSON.parse(localStorage.getItem("largeVehicles"))
      ? JSON.parse(localStorage.getItem("largeVehicles"))
      : [];

    if (smallVehiclesArray.length > 0 || mediumVehiclesArray.length > 0 || largeVehiclesArray.length > 0) {
      let newSystemInfo = {
        entryPoints: systemInfo.entryPoints,
        parkingSlots:
          systemInfo.parkingSlots -
          smallVehiclesArray.length -
          mediumVehiclesArray.length -
          largeVehiclesArray.length,
        smallVehicles: systemInfo.smallVehicles - smallVehiclesArray.length,
        mediumVehicles: systemInfo.mediumVehicles - mediumVehiclesArray.length,
        largeVehicles: systemInfo.largeVehicles - largeVehiclesArray.length,
      };

      dispatch(setSystemInfo(newSystemInfo));
    }
  }, []);
  return !isInitiated ? (
    <Container>
      <InputPage />
    </Container>
  ) : (
    <Router>
      <Container>
        <Routes>
          <Route exact path="/" element={<ParkingPage />} />
          <Route exact path="/home" element={<ParkingPage />} />
          <Route exact path="/parkVehicle" element={<ParkVehicleUI />} />
          <Route exact path="/unparkVehicle" element={<UnparkVehicleUI />} />
        </Routes>
      </Container>
    </Router>
  );
}

export default App;
