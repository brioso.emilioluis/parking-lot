import { createSlice } from "@reduxjs/toolkit";

export const systemInfoSlice = createSlice({
  name: "systemInformation",
  initialState: {
    entryPoints: 0,
    parkingSlots: 0,
    smallVehicles: 0,
    mediumVehicles: 0,
    largeVehicles: 0,
  },
  reducers: {
    setSystemInfo: (state, action) => {
      state.entryPoints = action.payload.entryPoints;
      state.parkingSlots = action.payload.parkingSlots;
      state.smallVehicles = action.payload.smallVehicles;
      state.mediumVehicles = action.payload.mediumVehicles;
      state.largeVehicles = action.payload.largeVehicles;
    },
    resetSystemInfo: (state) => {
      state.entryPoints = 0;
      state.parkingSlots = 0;
      state.smallVehicles = 0;
      state.mediumVehicles = 0;
      state.largeVehicles = 0;
    },
    parkSmallVehicle: (state) => {
      state.parkingSlots -= 1;
      state.smallVehicles -= 1;
    },
    unparkSmallVehicle: (state) => {
      state.parkingSlots += 1;
      state.smallVehicles += 1;
    },
    parkMediumVehicle: (state) => {
      state.parkingSlots -= 1;
      state.mediumVehicles -= 1;
    },
    unparkMediumVehicle: (state) => {
      state.parkingSlots += 1;
      state.mediumVehicles += 1;
    },
    parkLargeVehicle: (state) => {
      state.parkingSlots -= 1;
      state.largeVehicles -= 1;
    },
    unparkLargeVehicle: (state) => {
      state.parkingSlots += 1;
      state.largeVehicles += 1;
    },
  },
});

export const {
  setSystemInfo,
  resetSystemInfo,
  parkSmallVehicle,
  unparkSmallVehicle,
  parkMediumVehicle,
  unparkMediumVehicle,
  parkLargeVehicle,
  unparkLargeVehicle,
} = systemInfoSlice.actions;

export default systemInfoSlice.reducer;
