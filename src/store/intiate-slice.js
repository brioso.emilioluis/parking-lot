import { createSlice } from "@reduxjs/toolkit";

export const initiateSlice = createSlice({
  name: "initiate",
  initialState: {
    isInitiated: false,
  },
  reducers: {
    initiate: (state) => {
      state.isInitiated = true;
    },
    restart: (state) => {
      state.isInitiated = false;
    },
  },
});

export const { initiate, restart } = initiateSlice.actions;

export default initiateSlice.reducer;
