import React, { Fragment, useEffect, useState } from "react";
import { Table, Button, Modal } from "react-bootstrap";
import {
  unparkSmallVehicle,
  unparkMediumVehicle,
  unparkLargeVehicle,
} from "../store/system-info-slice";
import { useDispatch } from "react-redux";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

const ParkedVehiclesTable = () => {
  // declare redux hooks
  const dispatch = useDispatch();

  // declare react hooks
  const [vehicles, setVehicles] = useState([]);
  const [selectedVehicle, setSelectedVehicle] = useState(null);
  const [selectedVehicleType, setSelectedVehicleType] = useState(null);
  const [totalDuration, setTotalDuration] = useState(null);
  const [totalFare, setTotalFare] = useState(null);

  // Modal
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const navigate = useNavigate();

  useEffect(() => {
    const smallVehicles = JSON.parse(localStorage.getItem("smallVehicles"))
      ? JSON.parse(localStorage.getItem("smallVehicles"))
      : [];
    const mediumVehicles = JSON.parse(localStorage.getItem("mediumVehicles"))
      ? JSON.parse(localStorage.getItem("mediumVehicles"))
      : [];
    const largeVehicles = JSON.parse(localStorage.getItem("largeVehicles"))
      ? JSON.parse(localStorage.getItem("largeVehicles"))
      : [];

    setVehicles([...smallVehicles, ...mediumVehicles, ...largeVehicles]);
  }, [vehicles]);

  const handleUnpark = (vehicle, time, type) => {
    setSelectedVehicleType(type);
    setSelectedVehicle(vehicle);
    const timeIn = new Date(time);
    const timeOut = new Date();
    const duration = Math.abs(timeIn.getTime() - timeOut.getTime()) / 36e5;
    setTotalDuration(Math.round(duration));
    if (type === "small") {
      setTotalFare(40 + totalDuration * 20);
    } else if (type === "medium") {
      setTotalFare(40 + totalDuration * 60);
    } else if (type === "large") {
      setTotalFare(40 + totalDuration * 100);
    }
    handleShow();
  };

  const unparkVehicle = () => {
    let type = selectedVehicleType;
    let vehicle = selectedVehicle;

    if (type === "small") {
      let smallVehicles = JSON.parse(localStorage.getItem("smallVehicles"));
      let newSmallVehicles = smallVehicles.filter((item) => {
        return item.name !== vehicle;
      });
      localStorage.setItem("smallVehicles", JSON.stringify(newSmallVehicles));
      dispatch(unparkSmallVehicle());
    } else if (type === "medium") {
      let mediumVehicles = JSON.parse(localStorage.getItem("mediumVehicles"));
      let newMediumVehicles = mediumVehicles.filter((item) => {
        return item.name !== vehicle;
      });
      localStorage.setItem("mediumVehicles", JSON.stringify(newMediumVehicles));
      dispatch(unparkMediumVehicle());
    } else if (type === "large") {
      let largeVehicles = JSON.parse(localStorage.getItem("largeVehicles"));
      let newLargeVehicles = largeVehicles.filter((item) => {
        return item.name !== vehicle;
      });
      localStorage.setItem("largeVehicles", JSON.stringify(newLargeVehicles));
      dispatch(unparkLargeVehicle());
    }
    handleClose();
    Swal.fire({
      title: "Thank you for parking!",
      icon: "success",
    }).then((redirect) => {
      navigate("/");
    });
  };
  return (
    <Fragment>
      {/* Render Table */}
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Vehicle-ID</th>
            <th>Entry Point</th>
            <th>Time In</th>
            <th>Time Out</th>
            <th>Fare</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {vehicles.map((item) => (
            <tr>
              <td>{item.name}</td>
              <td>{item.entryPoint}</td>
              <td>{item.timeIn}</td>
              <td>{item.timeOut}</td>
              <td>{item.totalFare}</td>
              <td>
                <Button
                  onClick={() =>
                    handleUnpark(item.name, item.timeIn, item.type)
                  }
                >
                  Unpark
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {/* Render unpark modal */}
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Unpark Vehicle: {selectedVehicle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Total duration: {totalDuration}</h4>
          <h4>Total fare: {totalFare}</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Go Back
          </Button>
          <Button variant="primary" onClick={() => unparkVehicle()}>
            Unpark Vehicle
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

export default ParkedVehiclesTable;
