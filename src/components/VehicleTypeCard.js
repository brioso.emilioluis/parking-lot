import React, { Fragment, useEffect, useState } from "react";
import { Card, Button, Modal, ButtonGroup, Alert } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  parkSmallVehicle,
  parkMediumVehicle,
  parkLargeVehicle,
} from "../store/system-info-slice";
import Swal from "sweetalert2";
import nextId from "react-id-generator";
import { useNavigate } from "react-router-dom";

const VehicleTypeCard = ({ size, image, text }) => {
  // declare redux hooks
  const entryPoints = useSelector((state) => state.systemInfo.entryPoints);
  const smallVehicles = useSelector((state) => state.systemInfo.smallVehicles);
  const mediumVehicles = useSelector(
    (state) => state.systemInfo.mediumVehicles
  );
  const largeVehicles = useSelector((state) => state.systemInfo.largeVehicles);
  const dispatch = useDispatch();

  // declare react hooks
  const [sizeToShow, setSizeToShow] = useState("");
  const [slotsToShow, setSlotsToShow] = useState(null);
  const [entryPoint, setEntryPoint] = useState(null);

  // Modal
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const navigate = useNavigate();

  const parkVehicle = (sizeParam) => {
    // sets params for modal render
    setSizeToShow(sizeParam);
    if (sizeParam === "small") {
      setSlotsToShow(
        parseInt(smallVehicles) +
          parseInt(mediumVehicles) +
          parseInt(largeVehicles)
      );
    } else if (sizeParam === "medium") {
      setSlotsToShow(parseInt(mediumVehicles) + parseInt(largeVehicles));
    } else if (sizeParam === "large") {
      setSlotsToShow(parseInt(largeVehicles));
    }
    handleShow();
  };

  const setNewVehicle = () => {
    let newVehicle = {
      name: "",
      type: sizeToShow,
      entryPoint: entryPoint,
      timeIn: new Date(),
      timeOut: null,
      totalFare: 40,
    };

    const parkedSmallVehicles = JSON.parse(
      localStorage.getItem("smallVehicles")
    )
      ? JSON.parse(localStorage.getItem("smallVehicles"))
      : [];
    const parkedMediumVehicles = JSON.parse(
      localStorage.getItem("mediumVehicles")
    )
      ? JSON.parse(localStorage.getItem("mediumVehicles"))
      : [];
    const parkedLargeVehicles = JSON.parse(
      localStorage.getItem("largeVehicles")
    )
      ? JSON.parse(localStorage.getItem("largeVehicles"))
      : [];

    if (sizeToShow === "small") {
      let idSmall = nextId();
      newVehicle.name = `${sizeToShow}-${idSmall}`;
      parkedSmallVehicles.push(newVehicle);
      localStorage.setItem(
        "smallVehicles",
        JSON.stringify(parkedSmallVehicles)
      );
      dispatch(parkSmallVehicle());
    } else if (sizeToShow === "medium") {
      let idMedium = nextId();
      newVehicle.name = `${sizeToShow}-${idMedium}`;
      parkedMediumVehicles.push(newVehicle);
      localStorage.setItem(
        "mediumVehicles",
        JSON.stringify(parkedMediumVehicles)
      );
      dispatch(parkMediumVehicle());
    } else if (sizeToShow === "large") {
      let idLarge = nextId();
      newVehicle.name = `${sizeToShow}-${idLarge}`;
      parkedLargeVehicles.push(newVehicle);
      localStorage.setItem(
        "largeVehicles",
        JSON.stringify(parkedLargeVehicles)
      );
      dispatch(parkLargeVehicle());
    }

    setShow(false);
    Swal.fire({
      title: "Thank you for parking!",
      icon: "success",
    }).then((redirect) => {
      navigate("/");
    });
  };

  return (
    <Fragment>
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top" src={`${image}`} />
        <Card.Body>
          <Card.Title>{size}</Card.Title>
          <Card.Text>{text}</Card.Text>
          <Button variant="primary" onClick={() => parkVehicle(size)}>
            Select
          </Button>
        </Card.Body>
      </Card>

      {/* Render Modal */}
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Park Vehicle: {sizeToShow} vehicle</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          You've chosen to park a {sizeToShow} type of vehicle. There are
          currently {slotsToShow} slots available. Where are you coming from?
          <ButtonGroup aria-label="Basic example">
            <Button variant="secondary" onClick={() => setEntryPoint("A")}>
              Entry A
            </Button>
            <Button variant="secondary" onClick={() => setEntryPoint("B")}>
              Entry B
            </Button>
            <Button variant="secondary" onClick={() => setEntryPoint("C")}>
              Entry C
            </Button>
            {entryPoints === 4 ? (
              <Button variant="secondary" onClick={() => setEntryPoint("D")}>
                Entry D
              </Button>
            ) : entryPoints === 5 ? (
              <>
                <Button variant="secondary" onClick={() => setEntryPoint("D")}>
                  Entry D
                </Button>
                <Button variant="secondary" onClick={() => setEntryPoint("E")}>
                  Entry E
                </Button>
              </>
            ) : (
              <></>
            )}
          </ButtonGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Go Back
          </Button>
          <Button variant="primary" onClick={() => setNewVehicle()}>
            Park Vehicle
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

export default VehicleTypeCard;
