import { configureStore } from "@reduxjs/toolkit";
import intiateSlice from "../store/intiate-slice";
import systemInfoSlice from "../store/system-info-slice";

export const store = configureStore({
  reducer: {
    initiate: intiateSlice,
    systemInfo: systemInfoSlice,
  },
});
